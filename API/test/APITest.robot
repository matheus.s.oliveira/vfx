*** Settings ***
Library    RequestsLibrary
Library    Collections
Library    JSONLibrary
Resource    ../core/core.robot


*** Variables ***
${API_POST}    http://restful-booker.herokuapp.com
${JSONFile}    ${OUTPUT DIR}\\API\\data\\customer.json
&{headers}    Content-Type=application/json

*** Test Cases ***

Create new Customer
    Create Session    URL    ${API_POST}    headers=${headers}
    
    ${JSON}=    Load Json From File    ${JSONFile}
    ${Post_Session}=    POST On Session    URL    /booking/    json=${JSON}
    ${id}=    Return value    ${Post_Session.json()}   bookingid 

    Status Should Be    200    ${Post_Session}

    ${Get_Session}=    GET On Session    URL    /booking/${id}
    ${booking}=    Return value    ${Post_Session.json()}   booking

    ${fname}=    Return value    ${booking}   firstname
    ${name}=    Return value    ${JSON}    firstname
    Should Be Equal As Strings    ${name}    ${fname}

