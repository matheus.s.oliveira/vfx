*** Settings ***
Library            SeleniumLibrary
Library            String
Library            Collections


*** Keywords ***
Return value
    [Arguments]    ${Json}    ${Key}
    ${value}=    Get From Dictionary    ${Json}   ${Key}
    RETURN    ${value}
