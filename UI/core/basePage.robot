*** Settings ***
Library            SeleniumLibrary
Library            String
Library            Collections

Variables           ../datapool/environment.py

*** Variables ***
${timeoutSec}    80
${GlobalPrices}    0

*** Keywords ***
Open Application
    [Arguments]    ${Browser}
    [Documentation]   Generic test setup to Open Browser
    Run Keyword If    '${Browser}'=='firefox'     Open Browser    ${URL}    ${Browser}    options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors"); add_argument("--disable-notifications")
    ...     ELSE IF   '${Browser}'=='chrome'    Open Browser    ${URL}    ${Browser}    options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors"); add_argument("--disable-first-run-ui")
    ...     ELSE      Log       Open Application went wrong.
    Maximize Browser Window

End Application
    [Documentation]   Finish Application and reset Global Variables to Suite Tests
    Set Global Variable    ${GlobalCombination}    List
    Set Global Variable    ${GlobalPrices}    0
    Close Browser

Assert Strings
    [Arguments]     ${first-string}    ${second-string}
    [Documentation]    Compare two intergers.
    Should Be Equal As Strings    ${first-string}    ${second-string}
    
Click
    [Arguments]     ${locator}    ${timeout}=${timeoutSec}
    [Documentation]    Waits for an element identified by ``locator`` and Click on it.
    ...
    ...    *Input Arguments:*
    ...    | *Name*  | *Description* |
    ...    | locator | xpath of the element to click |
    ...    | timeout | OPTIONAL: time to wait element on screen |
    
    SeleniumLibrary.Wait Until Element Is Enabled    ${locator}     ${timeout}
    SeleniumLibrary.Click Element  ${locator}

Write
    [Arguments]    ${locator}    ${text}    ${timeout}=${timeoutSec}
    [Documentation]    Waits for an element identified by ``locator`` and writes ``text`` on it.
    ...
    ...    *Input Arguments:*
    ...    | *Name*   | *Description* |
    ...    | locator  | xpath of the element to write in |
    ...    | text     | text to write |
 
    SeleniumLibrary.Wait Until Page Contains Element    ${locator}    ${timeout}
    SeleniumLibrary.Clear Element Text    ${locator}
    SeleniumLibrary.Input Text    ${locator}    ${text}
