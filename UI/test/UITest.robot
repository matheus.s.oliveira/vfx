*** Settings ***
Resource     ../core/basePage.robot
Resource     ../page/searchPage.robot

Test Setup        Open Application    ${Browser}
Test Teardown     End Application

*** Variables ***
${Browser}             chrome

*** Test Cases ***
Check Title in Google
    Search for "VFX Financial"
    Check if title is "VFX Financial Plc"

