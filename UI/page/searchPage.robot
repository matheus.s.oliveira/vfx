*** Settings ***
Resource     ../core/basePage.robot
Variables    ../locators/search.py


*** Keywords ***

Search for "${nameToSearch}"
    Click    ${acceptTerms}
    Write    ${inputSearch}    ${nameToSearch}
    Press Keys     	${inputSearch}    RETURN

Check if title is "${title}"
    ${getTitle}=    Get Text    ${titleName}
    Assert Strings    ${title}    ${getTitle}