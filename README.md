# VFX

## API TEST

For API testing, I utilized Robot Framework along with RequestLibrary for making requests and verifying responses. Additionally, I employed JSONLibrary to handle JSON files.

Firstly, I prepared a JSON file named customer.json containing the payload required for the POST request. After sending the POST request to the designated URL, I checked the status code. Subsequently, I extracted the bookingId from the response and utilized it in the GET URL to ensure that the name received in the response matches the one created previously.

## UI TEST

For UI testing, I again utilized the Robot Framework with the Selenium library and the Page Object design pattern. I've been working with this standardization for a few years now, which ensures better readability and easier maintenance. I organized directories based on complexity levels. In the core directory, low-level keywords are optimized for better performance, while in the pages directory, keywords for assembling tests are found. At the root level of the tests directory, tests are created using Gherkin based on expected behavior outcomes.